﻿using System.Data;
using System.Windows;
using System.Windows.Data;

namespace WpfEmployee
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
            btnSave.IsEnabled = false;
            btnCancel.IsEnabled = false;
            grid1.IsEnabled = false;
            employeeInfoTableDataGrid.IsEnabled=true;
        }

        public EmpDS empDS { get; set; }

        /// <summary>
        /// Window Loaded with the DataBse  Records.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            empDS = (EmpDS)FindResource("empDS");
            // Load data into the table EmployeeInfoTable. You can modify this code as needed.
            EmpDSTableAdapters.EmployeeInfoTableTableAdapter empDSEmployeeInfoTableTableAdapter = new EmpDSTableAdapters.EmployeeInfoTableTableAdapter();
            empDSEmployeeInfoTableTableAdapter.Fill(empDS.EmployeeInfoTable);

            CollectionViewSource employeeInfoTableViewSource = (CollectionViewSource)(this.FindResource("employeeInfoTableViewSource"));
            employeeInfoTableViewSource.View.MoveCurrentToLast();

            //Move DataGrid to the selected item
            this.employeeInfoTableDataGrid.ScrollIntoView(employeeInfoTableViewSource.View.CurrentItem);
        }

        /// <summary>
        /// Creating the New Records to the DataBase
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            AEDButtons();

            empDS = (EmpDS)FindResource("empDS");
            DataRow data=empDS.EmployeeInfoTable.NewEmployeeInfoTableRow();
            empDS.EmployeeInfoTable.Rows.Add(data);

            //Clear Items on the window
            CollectionViewSource employeeInfoTableViewSource = (CollectionViewSource)(this.FindResource("employeeInfoTableViewSource"));
            employeeInfoTableViewSource.View.MoveCurrentToLast();

            //Moving to the last DataGrid to the selected item
            this.employeeInfoTableDataGrid.ScrollIntoView(employeeInfoTableViewSource.View.CurrentItem);
        }

        /// <summary>
        /// Editing the Database Records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            AEDButtons();
            
            empDS = (EmpDS)FindResource("empDS");
            int rowCount = empDS.EmployeeInfoTable.Rows.Count;
            if (rowCount <= 0)
            {
                MessageBox.Show("Their is no Data to Edit");
                return;
            }
        }

        /// <summary>
        /// Deleting the Records from the DataBase
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {            
            SCButtons();
            
            empDS = (EmpDS)FindResource("empDS");
            int rowCount = empDS.EmployeeInfoTable.Rows.Count;
            if (rowCount <= 0)
            {
                MessageBox.Show("Their is no Data to Deleting");
                return;
            }

            CollectionViewSource employeeInfoTableViewSource = (CollectionViewSource)(this.FindResource("employeeInfoTableViewSource"));
            DataRowView dataRowView=(DataRowView)employeeInfoTableViewSource.View.CurrentItem;

            MessageBoxResult result = MessageBox.Show("Do you want to delete..!", "Delete Row data", MessageBoxButton.YesNoCancel,MessageBoxImage.Warning);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    dataRowView.Delete();

                    EmpDS empDS_1 = (EmpDS)FindResource("empDS");

                    // Load data into the table EmployeeInfoTable. You can modify this code as needed.
                    EmpDSTableAdapters.EmployeeInfoTableTableAdapter empDSEmployeeInfoTableTableAdapter = new EmpDSTableAdapters.EmployeeInfoTableTableAdapter();
                    empDSEmployeeInfoTableTableAdapter.Update(empDS_1.EmployeeInfoTable);
                    
                    MessageBox.Show("Deleted Sucessfully.","Record Deleted",MessageBoxButton.OK,MessageBoxImage.Information);
                    break;                
                default:
                    empDS = (EmpDS)FindResource("empDS");
                    empDS.EmployeeInfoTable.RejectChanges();
                    break;

            }
        }

        /// <summary>
        /// Saving the data Records into the Database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            SCButtons();

            empDS = (EmpDS)FindResource("empDS");

            // Load data into the table EmployeeInfoTable. You can modify this code as needed.
            EmpDSTableAdapters.EmployeeInfoTableTableAdapter empDSEmployeeInfoTableTableAdapter = new EmpDSTableAdapters.EmployeeInfoTableTableAdapter();
            int result = empDSEmployeeInfoTableTableAdapter.Update(empDS.EmployeeInfoTable);
            if (result > 0)
            {
                MessageBox.Show("Saved","Save",MessageBoxButton.OK,MessageBoxImage.Information);
            }
            else
                MessageBox.Show("Can't Save! Try Agian....!");
        }

        /// <summary>
        /// Cancel the Database Editing values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            SCButtons();

            empDS = (EmpDS)FindResource("empDS");
            empDS.EmployeeInfoTable.RejectChanges();
        }

        /// <summary>
        /// Disable the Add New, Edit and Delete buttons
        /// </summary>
        public void AEDButtons()
        {
            btnAddNew.IsEnabled = false;
            btnEdit.IsEnabled = false;
            btnDelete.IsEnabled = false;
            btnSave.IsEnabled = true;
            btnCancel.IsEnabled = true;

            grid1.IsEnabled = true;
            employeeInfoTableDataGrid.IsEnabled = false;
        }

        /// <summary>
        /// Disable the Save and Clear buttons
        /// </summary>
        public void SCButtons()
        {
            btnAddNew.IsEnabled = true;
            btnEdit.IsEnabled = true;
            btnDelete.IsEnabled = true;
            btnSave.IsEnabled = false;
            btnCancel.IsEnabled = false;

            grid1.IsEnabled = false;
            employeeInfoTableDataGrid.IsEnabled = true;
        }
    }
}
